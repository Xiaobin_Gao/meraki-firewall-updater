$(document).ready(function() {
    

    $.post({
        url: '/getOrgNetworks',
        contentType: 'application/json',
        cache: false,
        success: function(result) {
            console.log(result)
            if (result['status'] == 'SUCCESS') {
                for (let nw in result['networks']) {
                    $("#nw").append(`<option value=${nw['id']}>${nw}</option>`);
                }
            } else {
                // TODO
                // $("html").hide();
            }
        }
    });

    $("#download_btn").click(function() {
        $.get({
            url: downloadURL,
            success: function(result) {
                let a = document.createElement('a');
                let url = window.URL.createObjectURL(new Blob([result]));
                a.href = downloadURL;
                $("body").append(a);
                a.click();
                a.remove();
                window.URL.revokeObjectURL(url);
            }
        });
    });

    $("#add_btn").click(function() {
        appendVarInp('', '');
    });

    $("#vars_in_div td:last-child").click(function() {
        $(this).parent().remove();
    });

    $("#proceed_btn").click(function() {
        let countInvalidInp = 0;
        if (!$("#nw").val()) {
            $("#nw").addClass('is-invalid');
            countInvalidInp++;
        }

        if (!isInpFileValid('f_inp')) {
            $('#f_inp').addClass('is-invalid');
            countInvalidInp++;
        }

        $("#nw, #f_inp").change(function() {
            if (!$(this).val()) {
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
            }
        });

        if (countInvalidInp > 0) {
            return;
        }

    });

    $("#var_required_inp").change(function() {
        if ($(this).is(':checked')) {
            $("#vars_div").show();
        } else {
            $("#vars_div").hide();
            resetVarInps();
        }
    });

    var downloadURL = '/downloadInFTemplate';
    var preLoadedVars = {
        'SERVERIP': '10.0.0.3',
        'SERVERIP110': '10.0.0.110',
        'PRINTERIP250': '10.0.0.250',
        'VLAN200/25': '10.0.1.0/25',
        'VLAN100/24': '10.0.0.0/24',
        '': ''
    };

    var appendVarInp = function(vName, vVal) {
        let varTr = `
            <tr>
                <td class="var"><input type="text" class="form-control" value="${vName}"></td>
                <td class="val"><input type="text" class="form-control" value="${vVal}"></td>
                <td class="bg-light"><span class="close">&times;</span></td>
            </tr>
            `;
        $("#vars_in_div").append($(varTr));
    }

    var resetVarInps = function() {
        $("#vars_in_div").empty();
        for (let k in preLoadedVars) {
            appendVarInp(k, preLoadedVars[k]);
        }
    }
    resetVarInps();

    var isInpFileValid = function(fileInpId) {
        let filename = $("#" + fileInpId).val().split(/(\\|\/)/g).pop();
        if (!filename.endsWith('.xlsx')) {
            return false;
        }
        return true;
    }
});