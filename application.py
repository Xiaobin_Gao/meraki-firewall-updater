from flask import Flask, request, render_template, jsonify, send_file
import requests
import os
import xlrd
from werkzeug.utils import secure_filename
import uuid


app = Flask(__name__)

MERAKI_API_KEY = os.environ['MERAKI_API_KEY']
MERAKI_API_BASE = 'https://n188.meraki.com/api/v0'

MERAKI_API_HEADERS = {
    'Content-Type': 'application/json',
    'X-Cisco-Meraki-API-Key': MERAKI_API_KEY
}

IN_F_HEADERS_MAPPER = {
    'Comment': 'comment', 'Policy': 'policy', 'Protocol': 'protocol',
    'Src Port': 'srcPort', 'Src Cidr': 'srcCidr', 'Dest Port': 'destPort',
    'Dest Cidr': 'destCidr', 'Syslog': 'syslogEnabled'
}

IN_FILE_FOLDER = './in_fs'


class Custom_Exp(Exception):
    pass


def read_firewall_rules_from_xlsx(path: str, vars: dict = {}):
    with xlrd.open_workbook(path) as f:
        f_sheet = f.sheet_by_index(0)
        n_cols = f_sheet.ncols
        n_rows = f_sheet.nrows
        if n_cols != len(IN_F_HEADERS_MAPPER):
            raise Custom_Exp(f'Invalid file')
        hdrs = {}
        for i in range(n_cols):
            hdr = str(f_sheet.cell_value(0, i)).strip()
            if hdr not in IN_F_HEADERS_MAPPER:
                raise Custom_Exp(f'Invalid file')
            else:
                hdrs[i] = IN_F_HEADERS_MAPPER[hdr]
        rs = []
        for j in range(1, n_rows):
            r = {}
            for k in range(n_cols):
                c_head = hdrs[k]
                c_val = str(f_sheet.cell_value(j, k)).strip()
                if c_head == 'syslogEnabled':
                    c_val = False if c_val == '0' else True
                r[c_head] = c_val if c_val not in vars else vars[c_val]
            rs.append(r)
        print('------rules--------')
        print(rs)
        return rs


# get ID of the first organization in the organizations fetched
def get_organization():
    url = f'{MERAKI_API_BASE}/organizations'
    resp = requests.get(url, headers=MERAKI_API_HEADERS)
    print('1------organization--------')
    print(resp.json())
    return resp.json()[0]['id']


# get IDs of all the networks under an organization
def get_networks(organization_id):
    url = f'{MERAKI_API_BASE}/organizations/{organization_id}/networks'
    resp = requests.get(url, headers=MERAKI_API_HEADERS)
    print('2------networks--------')
    print(resp.json())
    nws = {}
    for item in resp.json():
        nws[item['name']] = item['id'] 
    return nws
    # return resp.json()


# update L3 firewall rules for a network
def update_L3_firwall_rules(network_id, rules):
    url = f'{MERAKI_API_BASE}/networks/{network_id}/l3FirewallRules'
    data = {
        'rules': rules
    }
    resp = requests.put(url, headers=MERAKI_API_HEADERS, json=data)
    status_code = resp.status_code
    # print('3------rules--------')
    # print(status_code)
    if status_code == 200:
        return {
            'success': resp.json()
        }
    if status_code == 400:
        msg = resp.json()['errors'][0]
        if msg.startswith('At least one of your firewall rules is invalid'):
            raise Custom_Exp('At least one of your firewall rules is invalid')
    raise Exception('Please notify your administrator about this error')


@app.route('/getOrgNetworks', methods=['POST'])
def get_nets():
    org = get_organization()
    nws = get_networks(org)
    return jsonify({
        'status': 'SUCCESS',
        'networks': nws
    }) if len(nws) > 0 else jsonify({
        'status': 'ERROR'
    })


@app.route('/updateL3FirewallRules', methods=['POST'])
def update_fw():
    nw = request.args.get('nw')
    f = request.files['fw_rules_f']
    path_f = os.path.join(IN_FILE_FOLDER, str(
        uuid.uuid4()) + '_' + secure_filename(f.filename))
    f.save(path_f)
    try:
        rules = read_firewall_rules_from_xlsx(path_f)
    except Custom_Exp as e:
        return jsonify({
            'excp': str(e)
        })
    # except Exception as e:
    #     return jsonify({
    #         'excp': 'Please notify your administrator about this error'
    #     })
    finally:
        os.remove(path_f)


@app.route('/downloadInFTemplate')
def download_in_tmp():
    return send_file(os.path.join(IN_FILE_FOLDER, 'tmp', 'template.xlsx'), as_attachment=True, attachment_filename='MX_L3_Firwall_Rules.xlsx')


@app.route('/')
def index():
    return render_template('index.html')


if __name__ == '__main__':
    app.run(debug=True)
